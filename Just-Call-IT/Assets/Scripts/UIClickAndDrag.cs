﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIClickAndDrag : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    Vector3 startPosition, holder;

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        holder = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(holder.x, holder.y, 1);
    }
}
