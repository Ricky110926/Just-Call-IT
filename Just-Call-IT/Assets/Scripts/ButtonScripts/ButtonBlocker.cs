﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBlocker : MonoBehaviour
{
    public GameObject buttonBlocker, phoneDialBox, personDialBox, optionsButton;

    /* Start
     *   Disables the image GameObject that blocks the buttons.
     */
    void Start()
    {
        buttonBlocker.SetActive(false);
    }

    /* Update
     *   Calls BlockButtons() every frame.
     */
    void Update()
    {
        BlockButtons();
    }

    /* Block Buttons
     *   Enables the button blocker GameObject if anyone is speaking (i.e. the phone/person
     *   dialogue boxes are visible) or if the option buttons are visible. Enabling the
     *   button blocker stops the player from clicking on any buttons during dialogue and option
     *   selection. Otherwise, the button blocker is disabled.
     */
    public void BlockButtons()
    {
        if(phoneDialBox.activeInHierarchy == true || personDialBox.activeInHierarchy == true
            || optionsButton.activeInHierarchy == true)
        {
            buttonBlocker.SetActive(true);
        }
        else
        {
            buttonBlocker.SetActive(false);
        }
    }
}
