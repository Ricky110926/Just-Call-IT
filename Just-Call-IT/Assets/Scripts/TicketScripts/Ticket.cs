﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Ticket", menuName = "Ticket")]
public class Ticket : ScriptableObject
{
    public string header = "New Ticket";
    public string description = "New Description";
    public bool isComplete, isStored;
    public GameObject expandedTicket;
    private TextMeshProUGUI ticketChoices;

    public List<string> playerChoices = new List<string>();
    public List<string> criticisms = new List<string>();

    public void ResetTicket()
    {
        isComplete = false;
        isStored = false;
        playerChoices.Clear();
        criticisms.Clear();
    }

    /* Save Choice
     *   Parameter: string choice
     *   Adds the specified choice to the list of choices the player has made so far. Also adds
     *   that choice's criticism to the list of criticisms.
     */
    public void SaveChoice(string choice, string crit)
    {
        playerChoices.Add(choice);
        criticisms.Add(crit);
        Debug.Log(crit);
    }

    /* Display Info
     *   Displays the ticket's header, description, and saved player choices on an
     *   "expanded" ticket (which is a image object that looks like a blank ticket).
     */
    public virtual void DisplayInfo()
    {
        expandedTicket.SetActive(true);
        expandedTicket.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = header;
        expandedTicket.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = description;
        ticketChoices = expandedTicket.transform.GetChild(4).GetChild(0).GetComponent<TextMeshProUGUI>();
        for (int i = 0; i < playerChoices.Count; i++)
        {
            if (i == 0)
            {
                ticketChoices.richText = true;
                ticketChoices.text = playerChoices[i] + "\n<color=red>▲ " + criticisms[i] + "</color>\n";
            }
            else
            {
                ticketChoices.text += "\n" + playerChoices[i] + "\n<color=red>▲ " + criticisms[i] + "</color>\n";
            }
        }

        if (playerChoices.Capacity == 0)
        {
            ticketChoices.text = "None";
        }

        Debug.Log("Displaying Ticket info for " + name);
    }
}