﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimON : MonoBehaviour
{

    [SerializeField]private Animator anim;

    public string startClip;
    public string stopClip;

    public void Start()
    {
        //needs prefab to be first child in heirarchy
        anim = this.gameObject.transform.GetChild(0).GetComponent<Animator>();
    }


    public void PlayAnimation()
    {
        anim.Play(startClip);
    }

    public void StopAnimation()
    {
        anim.Play(stopClip);
    }
}
