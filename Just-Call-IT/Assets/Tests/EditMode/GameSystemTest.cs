﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class GameSystemTest
    {

        [Test]
        public void ResetsAndTotals()
        {
            //Assembly
            ScoreManager score = new ScoreManager();
            score.resetScore();
            score.resetTotalScore();
            //Act
            score.setScore(10);
            score.setTotalScore(score.getScore());
            //Assert
            Assert.AreEqual(expected: 10, actual: score.getTotalScore());

        }

        // A Test behaves as an ordinary method
        [Test]
        public void SetsandGets()
        {
            ScoreManager score = new ScoreManager();
            score.resetScore();
            int scoreInt = 100;
            score.setScore(scoreInt);

            Assert.AreEqual(expected: 100, actual: score.getScore());
            score.resetScore();
        }
    }
}
