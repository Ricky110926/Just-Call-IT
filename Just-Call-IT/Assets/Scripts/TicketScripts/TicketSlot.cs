﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TicketSlot : MonoBehaviour
{
    public Image ticketIcon;
    private Object ticketPic;
    Ticket ticket;

    /* Start
     *   Loads the blankTicket sprite for later use.
     */
    void Start()
    {
        ticketPic = Resources.Load("Images/Sprites/BulletinBoard/blankTicket", typeof (Sprite));      
    }

    /* Update
     *   Calls ToggleButton() every frame.
     */
    void Update()
    {
        ToggleButton();
    }

    /* Add Ticket
     *   Parameter: Ticket newTicket
     *   Sets the ticket in the ticket slot to the specified ticket, sets
     *   the slot's sprite to the sprite loaded earlier, and enables the image.
     */
    public void AddTicket(Ticket newTicket)
    {
        ticket = newTicket;

        ticketIcon.sprite = (Sprite)ticketPic;
        ticketIcon.enabled = true;
    }

    /* Clear Slot
     *   Sets the ticket in the ticket slot to null, sets the slot's sprite
     *   to null, and disables the image.
     */
    public void ClearSlot()
    {
        ticket = null;

        ticketIcon.sprite = null;
        ticketIcon.enabled = false;
    }

    /* Display Ticket
     *   Displays the ticket's information if the ticket in the ticket slot 
     *   is not null.
     */
    public void DisplayTicket()
    {
        if(ticket != null)
        {
            ticket.DisplayInfo();
        }
    }

    /* Toggle Button
     *   Disables the ticket slot's button if the slot is empty (the image is
     *   disabled), and vice versa.
     */
    public void ToggleButton()
    {
        if(!ticketIcon.enabled)
        {
            this.transform.GetComponentInChildren<Button>().enabled = false;
        }
        else
        {
            this.transform.GetComponentInChildren<Button>().enabled = true;
        }
    }
}
