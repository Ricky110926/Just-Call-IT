﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAndDrag : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private bool isDragging;
    public bool ticketCanDrag;

    private Vector2 mousePosition;

    /* Update
     *   Checks if the ticketCanDrag boolean is false. If so, this script do anything until it is true.
     *   Checks if the isDragging boolean is true. If so, the player's mouse position will
     *   be saved and the ticket can be moved based on the saved mouse position. Basically,
     *   the ticket is dragged based on the mouse position.
     */
    void Update()
    {
        if(!ticketCanDrag)
        {
            // stops the "click and drag" from happening until the ticket is complete
        }
        else if (isDragging)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.localPosition;
            transform.Translate(new Vector2(mousePosition.x - startPosX, mousePosition.y - startPosY));
        }
    }

    /* On Mouse Down
     *   While the left mouse button is being pressed, isDragging is set to true and the
     *   mouse position is saved. The ticket is able to be dragged.
     */
    public void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;

            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            startPosX = mousePosition.x - this.transform.localPosition.x;
            startPosY = mousePosition.y - this.transform.localPosition.y;
        }
    }

    /* On Mouse Up
     *   When the left mouse button is released/not being pressed, isDragging is set to
     *   false. The ticket is not able to be dragged.
     */
    public void OnMouseUp()
    {
        isDragging = false;
    }

    /* SetTicketToDrag
     *   Sets the ticketCanDrag boolean to true, so this script can being the process of
     *   saving mouse positions to allow ticket dragging.
     */
    public void SetTicketToDrag()
    {
        ticketCanDrag = true;
    }
}
