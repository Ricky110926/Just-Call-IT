﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonActivation : MonoBehaviour
{
    public Dialogue activatingDial, deactivatingDial, lastDial;
    public Button button;
    bool interactButton = false;
    string phoneButton = "PhoneButton";
    public GameObject fader;
    public GameObject lastTicket;
    ColorBlock originalButtonColors, newButtonColors;

    /* Start
     *   Disables the button this script is attached to unless it is the button labeled as 
     *   "PhoneButton" (in which case the button is set to non-interactable). Sets the
     *   interactButton boolean based on whether or not the button is the "PhoneButton."
     */
    void Start()
    {
        if(button.name == phoneButton)
        {
            button.interactable = false;
            interactButton = true;
            originalButtonColors = button.colors;
        }
        else
        {
            button.enabled = false;
            interactButton = false;
        }
    }

    /* Update
     *   Checks to see if a specific dialogue is finished in order to activate a specific button,
     *   then ends the scene after all button interactions are complete.
     */
    void Update()
    {
        if (interactButton)
        {
            SwitchOverlayButton(activatingDial, deactivatingDial, button);
        }
        else
        {
            SwitchButton(activatingDial, deactivatingDial, button);
        }

        EndScene();
    }

    /*Switch Button
     *  Parameters: Dialogue prevDial, Dialogue thisDial, Button button
     *  Activates/Deactivates the specified button if the specified dialogues are complete.
     */
    public void SwitchButton(Dialogue prevDial, Dialogue thisDial, Button button)
    {
        // if the dialogue is complete, enable the button
        if (prevDial.DialogueStatus())
        {
            button.enabled = true;
        }

        if ((!prevDial.DialogueStatus() || thisDial.DialogueStatus()) && button.enabled == true)
        {
            /* calls DeactivateButton on the specified button if the dialogue is not
               complete and the button is enabled */
            DeactivateButton(button);
        }
    }

    /* Deactivate Button
     *   Parameter: Button button
     *   Disables the specified button.
     */   
    public void DeactivateButton(Button button)
    {
        button.enabled = false;
    }

    /* Switch Overlay Button
     *   Parameters: Dialogue prevDial, Dialogue thisDial, Button button
     *   Same as SwitchButton() but is used for buttons that are overlayed over images
     *   (i.e. buttons that set interactButton to true). Changes the button's highlight,
     *    pressed, and selected colors to their original values so that visual feedback
     *    displays when the player tries to interact with the activated button.
     */
    public void SwitchOverlayButton(Dialogue prevDial, Dialogue thisDial, Button button)
    {
        if (prevDial.DialogueStatus())
        {
            button.interactable = true;
            button.colors = originalButtonColors;
        }

        if ((!prevDial.DialogueStatus() || thisDial.DialogueStatus()) && button.interactable == true)
        {
            DeactivateOverlayButton(button);
        }
    }

    /* Deactivate Overlay Button
     *   Parameter: Button button
     *   Same as DeactivateButton() but is used for buttons that are overlayed over images
     *   (i.e. buttons that set interactButton to true). Changes the button's highlight,
     *   pressed, and selected colors to transparent, so when the player trys to interact
     *   with the deactivated button, no visual feedback will be displayed.
     */   
    public void DeactivateOverlayButton(Button button)
    {
        button.interactable = false;
        Color transparent = new Color(0, 0, 0, 0);
        newButtonColors = originalButtonColors;
        newButtonColors.highlightedColor = transparent;
        newButtonColors.pressedColor = transparent;
        newButtonColors.selectedColor = transparent;
        button.colors = newButtonColors;
    }

    /* End Scene
     *   If the ticket is set to completed through the "CompletedTicket" script, the
     *   ticket isn't visible on screen, and the last dialogue is set to complete, fade to
     *   the fourth scene in the scene manager using the "Fader" script.
     */
    //public void EndScene()
    //{
    //    if (bulletinTicket.GetComponent<CompletedTicket>().isComplete == true)
    //    {
    //        if (bulletinTicket.activeInHierarchy == false && lastDial.isComplete == true)
    //        {
    //            fader.GetComponent<Fader>().FadeToLevel(3);
    //        }
    //    }
    //}

    public void EndScene()
    {
        if (lastTicket.GetComponent<TicketInteract>().ticket.isComplete == true)
        {
            if (lastTicket.activeInHierarchy == false && lastDial.isComplete == true)
            {
                lastTicket.GetComponent<TicketInteract>().ticket.isComplete = false;
                lastTicket.GetComponent<TicketInteract>().ticket.isStored = false;
                if (fader.GetComponent<Fader>().GetCurrentLevelIndex() == 2)
                {
                    fader.GetComponentInParent<LevelTracker>().TutorialComplete(true);
                    fader.GetComponent<Fader>().FadeToNextLevel();
                }
                else if(fader.GetComponent<Fader>().GetCurrentLevelIndex() == 4)
                {
                    fader.GetComponent<Fader>().FadeToLevel(3);
                }
            }
        }
    }
}
