﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DimmerHider : MonoBehaviour
{
    public Image dimmer, phoneDialBox;

    /* Start
     *   Hides the dimmer object.
     */
    void Start()
    {
        dimmer.gameObject.SetActive(false);
    }

    /* Update
     *   Shows the dimmer if the someone is talking from the phone (i.e. the phone
     *   dialogue box is visible), otherwise hides the dimmer.
     */
    void Update()
    {
        if (phoneDialBox.IsActive() == true)
        {
            //dimmer.gameObject.SetActive(true);
            ToggleDimmer(dimmer, true);
        }
        else
        {
            //dimmer.gameObject.SetActive(false);
            ToggleDimmer(dimmer, false);
        }
    }

    /* Toggle Dimmer
     *   Parameters: Image dim, boolean toggle
     *   Hides or shows the passed in image based on the passed in boolean.
     */
    public void ToggleDimmer(Image dim, bool toggle)
    {
        dim.gameObject.SetActive(toggle);
    }
}
