﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class SoundController : MonoBehaviour
{
    public AudioSource soundSource;
    // Start is called before the first frame update
    void Start()
    {
        soundSource = gameObject.GetComponent<AudioSource>();
    }

    public void startSound()
    {
        soundSource.Play();
    }

    public void stopSound()
    {
        soundSource.Stop();
    }
}
