﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTracker : MonoBehaviour
{
    private int tutorial = 2, level1 = 4, thanksPlaying = 5;
    private int prevLevel;
    private bool tutComplete;

    [SerializeField]
    private ScoreManager scoreManager;

    public void SetLeavingLevel()
    {
        PlayerPrefs.SetInt("leavingLevel", SceneManager.GetActiveScene().buildIndex);
    }

    public int GetLeavingLevel()
    {
        return PlayerPrefs.GetInt("leavingLevel");
    }

    public void ResetLeavingLevel()
    {
        PlayerPrefs.DeleteKey("leavingLevel");
    }

    public int ScoreLevelSwitch()
    {
        int level;
        prevLevel = GetLeavingLevel();

        if(prevLevel == tutorial)
        {
            level = level1;
        }
        else
        {
            level = thanksPlaying;
        }

        return level;
    }

    public void DeleteLevelOneScore()
    {
        if(IsTutorialComplete())
        {
            scoreManager.removeLevelScore();
        }
    }

    public bool IsTutorialComplete()
    {
        return System.Convert.ToBoolean(PlayerPrefs.GetString("TutComplete"));
    }

    public void TutorialComplete(bool status)
    {
        tutComplete = status;
        PlayerPrefs.SetString("TutComplete", tutComplete.ToString());
    }
}
