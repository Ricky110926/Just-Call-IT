﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public static bool paused = false;

    public GameObject pauseMenu;

    /* Update
     *   Checks if the excape key was pressed, and calls a function based on the status of
     *   the "paused" boolean.
     */
    void Update()
    {
        // checks if the excape key was pressed
        if (Input.GetKeyDown("escape"))
        {
            if(paused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    /* Resume
     *   Hides the pause menu, resumes the game by unfreezing time, and sets the paused
     *   to false.
     */
    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        paused = false;
    }

    /* Pause
     *   Displays the pause menu, pauses the game by freezing time, and sets the paused
     *   boolean to true. 
     */
    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        paused = true;
    }

    /* Save game
     *   to be determined
     */
    public void SaveGame()
    {
        // to be determined
        Debug.Log("Saving Game...");
    }

    /* Load Game
     *   to be determined
     */
    public void LoadGame()
    {
        // to be determined
        Debug.Log("Loading Game...");
    }

    /* Settings
     *   to be determined
     */
    public void Settings()
    {

    }

    /* Quit To Main Menu
     *   Resumes the game and then quits through the hierarchy.
     */
    public void QuitToMainMenu()
    {
        Resume();
    }
}
