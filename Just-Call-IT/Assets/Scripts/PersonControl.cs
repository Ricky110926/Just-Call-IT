﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonControl : MonoBehaviour
{
    private Animator animator;
    public ConversationManager convoManager;
    private Object taylor, karen;

    /* Start
     *   Disables the Animator for Taylor and saves the first image in the Taylor sprite set.
     */
    private void Start()
    {
        animator = GetComponent<Animator>();
        animator.enabled = false;
        taylor = Resources.Load("Images/Sprites/people/female-1", typeof(Sprite));
        karen = Resources.Load("Images/Sprites/people/karen", typeof(Sprite));
    }

    /* Update
     *   Calls StartStopAnimation every frame.
     */
    private void Update()
    {
        StartStopAnimation();
    }

    /* StartStopAnimation
     *   Starts or stops Taylor's animation based on whether or not she is currently talking.
     */
    public void StartStopAnimation()
    {
        // if there is a current dialogue
        if (convoManager.currentDialogue != null)
        {
            // if the current dialogue is from Taylor and it isn't marked as complete yet
            if ((convoManager.currentDialogue.Speaker == "Taylor"
                || convoManager.currentDialogue.Speaker == "Karen") && !convoManager.currentDialogue.isComplete)
            {
                // enable the animator (start the animation)
                animator.enabled = true; 
            }
            else
            {
                // disable the animator (stop the animation)
                animator.enabled = false;

                if (GetComponent<Image>().sprite.name.Contains("female"))
                {
                    // set the taylor's sprite to the first in her sprite set
                    GetComponent<Image>().sprite = (Sprite)taylor;
                }
                else
                {
                    // set the taylor's sprite to the first in her sprite set
                    GetComponent<Image>().sprite = (Sprite)karen;
                }
            }
        }
    }
}
