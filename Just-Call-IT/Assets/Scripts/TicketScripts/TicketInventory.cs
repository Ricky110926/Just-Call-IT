﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketInventory : MonoBehaviour
{
    #region Singleton

    public static TicketInventory instance;

    /* Awake
     *   Stops more than one instance of the ticket inventory from being created.
     */
    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance of TicketInventory found!");
            return;
        }

        instance = this;
    }

    #endregion

    public delegate void OnTicketChanged();
    public OnTicketChanged onTicketChangedCallback;

    public int capacity = 16;

    public List<Ticket> tickets = new List<Ticket>();

    /* Add
     *   Parameter: Ticket ticket
     *   Adds the specified ticket to the inventory's ticket array and calls the
     *   delegate, which will update the ticket slot UI. Sends a debug statement if
     *   there is no room in the ticket array for the specified ticket.
     */
    public bool Add(Ticket ticket)
    {
        if(tickets.Count >= capacity)
        {
            Debug.Log("Not enough room.");
            return false;
        }

        tickets.Add(ticket);

        if (onTicketChangedCallback != null)
        {
            onTicketChangedCallback.Invoke();
        }

        return true;
    }

    /* Remove
     *   Parameter: Ticket ticket
     *   Removes the specified ticket from the inventory's ticket array and calls
     *   the delegate, which will update the ticket slot UI.
     */
    public void Remove(Ticket ticket)
    {
        tickets.Remove(ticket);

        if (onTicketChangedCallback != null)
        {
            onTicketChangedCallback.Invoke();
        }
    }
}
