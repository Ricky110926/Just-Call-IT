﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KarenLeave : MonoBehaviour
{
    public Dialogue lastDial;

    // Update is called once per frame
    void Update()
    {
        if(lastDial.isComplete)
        {
            gameObject.SetActive(false);
        }
    }
}
