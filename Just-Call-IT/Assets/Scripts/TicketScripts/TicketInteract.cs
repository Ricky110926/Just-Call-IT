﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TicketInteract : MonoBehaviour
{
    public Ticket ticket;

    public Dialogue lastDial;
    public OptionManager optionManager;
    public Button bulButton;
    public GameObject threshold;
    public GameObject ticketInfo;
    private int ticketNum;
    private float thresholdXPos;
    private bool wasStored;
    private Object bulImage;

    /* Start
     *   Increments the number of total tickets, sets the current ticket and marks
     *   it as incomplete, clears the ticket's list of saved choices, sets the
     *   bulletin board image to the appropriate number of tickets, and stores
     *   an updated version of the threshold's x position for later use.
     */
    void Start()
    {
        gameObject.GetComponent<Image>().enabled = false;
        gameObject.GetComponentInParent<AllTickets>().IncTotalTickets();
        optionManager.currentTicket = ticket;
        ticket.ResetTicket();
        ticketNum = GTT();

        if (ticketNum <= 5)
        {
            bulImage = Resources.Load("Images/Sprites/BulletinBoard/bulletinboard-"
                + ticketNum, typeof(Sprite));
        }
        else if (ticketNum > 5)
        {
            bulImage = Resources.Load("Images/Sprites/BulletinBoard/bulletinboard-"
                + "toomany", typeof(Sprite));
        }

        bulButton.GetComponent<Image>().sprite = (Sprite)bulImage;

        thresholdXPos = threshold.transform.position.x;
    }

    /* Update
     *   Trys to set the ticket to complete every frame. Also, if the ticket is
     *   complete but not yet stored, trys to add the ticket to the ticket board
     *   every frame.
     */
    void Update()
    {
        SetToComplete();

        if (ticket.isComplete && !wasStored)
        {
            MoveToComplete();
        }
    }

    /* Get Total Ticket Num
     *   Debug statement showing the total number of tickets so far.
     */
    public void GetTotalTicketNum()
    {
        Debug.Log("total ticket num = " + gameObject.GetComponentInParent<AllTickets>().GetTotalTickets());
    }

    /* GTT
     *   Returns: The number of total tickets so far
     */
    public int GTT()
    {
        return gameObject.GetComponentInParent<AllTickets>().GetTotalTickets();
    }

    /* Get Complete Ticket Num
     *   Debug statement showing the number of completed tickets so far.
     */
    public void GetCompleteTicketNum()
    {
        Debug.Log("complete ticket num = " + gameObject.GetComponentInParent<AllTickets>().GetCompleteTicket());
    }

    /* Get Header
     *   Debug statement showing the ticket's header. 
     */
    public void GetHeader()
    {
        Debug.Log(ticket.header);
        
    }

    /* Get Description
     *   Debug statement showing the ticket's description.
     */
    public void GetDescription()
    {
        Debug.Log(ticket.description);
    }

    /* Set To Complete
     *   Sets the ticket to complete and increments to number of completed tickets
     *   if the last dial is complete and the ticket isn't already marked as complete.
     */
    public void SetToComplete()
    {
        if (lastDial.DialogueStatus() && ticket.isComplete == false)
        {
            ticket.isComplete = true;
            gameObject.GetComponentInParent<AllTickets>().IncCompleteTickets();
        }
    }

    /* Ticket Status
     *   Debug statement showing the whether the ticket is marked as completed or not.
     */
    public void TicketStatus()
    {
        Debug.Log("complete? -> " + ticket.isComplete);
    }

    /* Print Saved Choices
     *   Debug statement showing each of the saved player choices.
     */
    public void PrintSavedChoices()
    {
        for (int i = 0; i < ticket.playerChoices.Count; i++)
        {
            Debug.Log(ticket.playerChoices[i] + '\n');
        }
    }

    /* Move To Complete
     *   Adds the ticket to the ticket inventory (if there is space), hides the
     *   ticket, and sets the viewable ticket information gameobject.
     */
    public void MoveToComplete()
    {
        /* How to drag and drop the ticket object into the completed grid:
         *   Save the position of the left side of the completed ticket grid and when
         *   the mouse position crosses it, the ticket shows up in the first free slot
         *   and the ticket object is set inactive.
         */
        if (this.transform.position.x > thresholdXPos)
        {
            wasStored = TicketInventory.instance.Add(ticket);
            gameObject.SetActive(false);
            ticket.expandedTicket = ticketInfo;
            ticket.isStored = true;
        }
    }
}
