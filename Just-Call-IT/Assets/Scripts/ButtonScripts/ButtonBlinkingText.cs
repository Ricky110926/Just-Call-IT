﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBlinkingText : MonoBehaviour
{
    TextMeshProUGUI text;
    public Button startButton;

    /* Start
     *   Starts the blinking if the start button is not active.
     */
    void Start()
    {
        //text = GetComponent<TextMeshProUGUI>(); // removed this to fix NullReferencePointer error
        if (!startButton.IsActive())
        {
            StartBlinking();
        }
    }

    /* Blink
     *   I found this online and converted it from a blinking text object to a blinking TMP object.
     *   Switches the alpha of the TMP object between 1 (fully visible) and 0 (fully invisible)
     *   after a period of time (1 second).
     */
    IEnumerator Blink()
    {
        text = GetComponent<TextMeshProUGUI>(); // put this here to fix NullReferencePointer error
        while (true)
        {
            switch (text.color.a.ToString())
            {
                case "0":
                    text.color = new Color(text.color.r, text.color.b, text.color.b, 1);
                    yield return new WaitForSeconds(1f);
                    break;
                case "1":
                    text.color = new Color(text.color.r, text.color.b, text.color.b, 0);
                    yield return new WaitForSeconds(1f);
                    break;
            }
        }
    }

    /* Start Blinking
     *   Stops and starts the above Blink() coroutine.
     */
    public void StartBlinking()
    {
        StopCoroutine("Blink");
        StartCoroutine("Blink");
    }

    /* Stop Blinking
     *   Stops the above Blink() coroutine.
     */
    public void StopBlinking()
    {
        StopCoroutine("Blink");
    }
}