﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketToPerson : MonoBehaviour
{
    public Ticket doneTicket, newTicket;
    public OptionManager optionManager;
    public GameObject textBubble, person, bulletin, ticketImage;
    private bool checkpoint;

    private void Update()
    {
        if (!checkpoint)
        {
            DisplayPerson();
        }
    }

    public void DisplayPerson()
    {
        if(doneTicket.isComplete && doneTicket.isStored && !person.activeInHierarchy)
        {
            person.SetActive(true);
            textBubble.SetActive(true);
            ticketImage.SetActive(true);
            optionManager.currentTicket = newTicket;
            checkpoint = true;
        }
        else if(!doneTicket.isComplete && !doneTicket.isStored)
        {
            this.enabled = false;
        }
    }
}
