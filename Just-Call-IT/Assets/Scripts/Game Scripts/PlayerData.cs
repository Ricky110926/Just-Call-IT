﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData : MonoBehaviour
{
    public int level;
    public int totalScore;
    public string PlayerName;
    
    public PlayerData(Player player)
    {
        PlayerName = player.playerName;
        level = player.level;
        totalScore = player.totalScore;
    }

    public void setLevel(int indexBuild)
    {
        level = indexBuild;
    }
}
