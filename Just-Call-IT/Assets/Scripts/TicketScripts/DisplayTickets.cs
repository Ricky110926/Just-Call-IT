﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTickets : MonoBehaviour
{
    public GameObject ticketBoard;

    /* Start
     *   Disables all ticket images.
     */
    void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).gameObject.GetComponent<Image>().enabled = false;
        }
    }

    /* Update
     *   Enables all ticket images if the ticket board is visible, and vice versa.
     */
    void Update()
    {
        if(ticketBoard.activeSelf)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                if(gameObject.transform.GetChild(i).gameObject.activeSelf)
                {
                    gameObject.transform.GetChild(i).gameObject.GetComponent<Image>().enabled = true;
                }
            }
        }
        else
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                if (gameObject.transform.GetChild(i).gameObject.activeSelf)
                {
                    gameObject.transform.GetChild(i).gameObject.GetComponent<Image>().enabled = false;
                }
            }
        }
    }
}
