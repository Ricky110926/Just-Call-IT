﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllTickets : MonoBehaviour
{
    private int totalTickets;
    private int numTicketsCompleted;

    /* Inc Total Tickets
     *   Increments the number of total tickets by 1.
     */
    public void IncTotalTickets()
    {
        totalTickets += 1;
    }

    /* Get Total Tickets
     *   Returns: The number of total tickets
     */
    public int GetTotalTickets()
    {
        return totalTickets;
    }

    /* Inc Complete Tickets
     *   Increments the number of completed tickets by 1.
     */
    public void IncCompleteTickets()
    {
        numTicketsCompleted += 1;
    }

    /* Get Complete Ticket
     *   Returns: The number of completed tickets
     */
    public int GetCompleteTicket()
    {
        return numTicketsCompleted;
    }
}
