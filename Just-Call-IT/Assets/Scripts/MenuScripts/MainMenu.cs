﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Fader Fader;
    public Button level1;

    [SerializeField]
    private LevelTracker levelTracker;

    private void Update()
    {
        EnableLevelOne();
    }

    /* Play Game
     *   Loads the second scene (first is 0) in the scene manager, which is the transition scene.
     */
    public void PlayGame()
    {
        Fader.FadeToLevel(1);
    }

    /* Back To Main Menu
     *   Loads the first scene in the scene manager, which is the main menu.
     */
    public void BackToMainmenu()
    {
        Fader.FadeToLevel(0);
    }

    /* Quit Game
     *   Prints a quit message to the console in the unity player, but completely quits
     *   the game in the build.
     */
    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void EnableLevelOne()
    {
        level1.interactable = levelTracker.IsTutorialComplete();
    }
}
