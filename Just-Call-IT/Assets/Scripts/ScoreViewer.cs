﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Animations;

public class ScoreViewer : MonoBehaviour
{
    public TextMeshProUGUI levelScoreText;
    public TextMeshProUGUI totalScoreText;
    public TextMeshProUGUI levelScore;
    public TextMeshProUGUI totalScore;
    public TextMeshProUGUI clickToContinue;
    //public Image fader;
    public GameObject fader;

    public int lScore;
    public int lScoreCap;
    public int tScore;
    private int tScoreCap;
    public int scoreInc;

    [SerializeField]
    private ScoreManager scoreSystem;

    [SerializeField]
    private LevelTracker levelTracker;

    /* Start
     *   Sets all the TMP objects to false, and sets the updated total score.
     */
    void Start()
    {
        levelScoreText.gameObject.SetActive(false);
        levelScore.gameObject.SetActive(false);
        totalScoreText.gameObject.SetActive(false);
        totalScore.gameObject.SetActive(false);
        clickToContinue.gameObject.SetActive(false);

        // Set the new total score (current total score + level score = new total score)
        lScoreCap = PlayerPrefs.GetInt("LevelScore");
        Debug.Log(lScoreCap); 
        scoreSystem.setTotalScore(lScoreCap);
        tScoreCap = PlayerPrefs.GetInt("TotalScore");
        Debug.Log(tScoreCap);
    }

    /* Update
     *   Trys to fade in all the TMP objects and increase the scores every frame.
     */
    void Update()
    {
        StartFadeIn(levelScoreText, levelScoreText);
        StartFadeIn(levelScoreText, levelScore);

        IncreaseScores();

        StartFadeIn(totalScoreText, totalScore);

        ShowClickToContinue();
    }

    /* Start Fade In
     *   Parameters: TextMeshProUGUI visibleText, TextMeshProUGUI fadingText
     *   Fades in the specified text (fadingText) once the other specified text (visibleText)
     *   is fully visible.
     */
    public void StartFadeIn(TextMeshProUGUI visibleText, TextMeshProUGUI fadingText)
    {
        // the first condition is only used for the first call to StartFadeIn
        if (fadingText == levelScoreText || visibleText.alpha == 1)
        {
            fadingText.gameObject.SetActive(true);
        }
    }

    /* Increase Scores
     *  Increases the scores using a visible effect
     */
    public void IncreaseScores()
    {

        // converts the ints to strings so they can be displayed
        levelScore.text = lScore.ToString();
        totalScore.text = tScore.ToString();

        // makes the level score only begin increasing at the right time
        if (levelScore.alpha == 1 && totalScore.alpha == 0) 
        {
            // increases the level score by some increment until it reaches it's desired value
            if (lScore != lScoreCap)
            {
                lScore = lScore + scoreInc;
            }
        }
        else if (totalScore.alpha == 1)  // makes the total score only begin increasing at the right time
        {
            // increases the total score by some increment until it reaches it's desired value
            if (tScore != tScoreCap)
            {
                tScore = tScore + scoreInc;
            }
        }

        // only makes the total score text visible when the level score is done increasing
        if(lScore == lScoreCap)
        {
            StartFadeIn(levelScore, totalScoreText);
        }
    }

    /* Show Click To Start
     *   Shows the "Click Anywhere To Continue" Text and allows the scene to change once all scores
     *   are displayed.
     */
    public void ShowClickToContinue()
    {
        // checks if the total score is done increasing
        if(tScore == tScoreCap)
        {
            clickToContinue.gameObject.SetActive(true);

            // makes the text blink
            clickToContinue.GetComponent<BlinkingText>().enabled = true;

            if (Input.GetMouseButtonDown(0))
            {
                // fades to the next scene on left mouse button click
                fader.GetComponent<Fader>().FadeToLevel(levelTracker.ScoreLevelSwitch());
            }
        }
    }
}
