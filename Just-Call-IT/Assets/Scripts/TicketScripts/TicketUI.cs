﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketUI : MonoBehaviour
{
    public Transform ticketParent;

    TicketInventory inventory;

    TicketSlot[] slots;

    /* Start
     *   Makes an instance of the TicketInventory, sets that instance's delegate to the
     *   UpdateUI function below, and sets the TicketSlot array.
     */
    void Start()
    {
        inventory = TicketInventory.instance;
        inventory.onTicketChangedCallback += UpdateUI;
        slots = ticketParent.GetComponentsInChildren<TicketSlot>();
    }

    /* Update UI
     *   Loops through each ticket slot and adds the newest ticket (from the ticket
     *   invertory) to the next available slot if there is space for it. If there
     *   isn't space for the ticket, then the slot is cleared.
     */
    void UpdateUI()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.tickets.Count)
            {
                slots[i].AddTicket(inventory.tickets[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
